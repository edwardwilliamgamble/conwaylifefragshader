//#version 150
#version 110
//#version 130

#ifdef GL_ES
precision highp float;
#endif

uniform float time;
uniform vec2 resolution;
uniform vec4 mouse;
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform int mode;

void main(void){
  gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;
}
