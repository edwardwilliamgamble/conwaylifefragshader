#version 150
//#version 110
//#version 130

#ifdef GL_ES
precision highp float;
#endif

uniform float time;
uniform vec2 resolution;
uniform vec4 mouse;
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform int rmode;  //-1 = generate playfield from scratch, 
                    //0 = process row pairs without an offset (row 0 and row 1 can switch values, 2 and 3 can switch, ...)
                    //1 = process row pairs without an offset of 1(row 0 cannot change, row 1 and row 2 can switch values, 3 and 4 can switch, ...)
                    //no other values order goes -1,0,1,0,1,0,1,.....

vec4 water = vec4(0.0,0.1,0.9,0.95);
vec4 sand = vec4(0.6,0.2,0.2,1.0);
vec4 air = vec4(0.0,0.0,0.0,0.9);

vec4 off = vec4(0.0,0.0,0.0,1.0);
vec4 on = vec4(1.0,1.0,1.0,1.0);

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec4 generate(){
  return air;
}

void main(void){
  vec2 screenFrac = gl_FragCoord.xy/resolution;
  vec2 screenCoord = vec2(gl_FragCoord.x/resolution.y,gl_FragCoord.y/resolution.y);
  vec2 screenCoordCentered = vec2((gl_FragCoord.x-(resolution.x/2.0))/resolution.y,(gl_FragCoord.y-(resolution.y/2.0))/resolution.y);
  vec2 eyePos = vec2(0.0,0.0);
  float eyeVertFov = 1.0;

  vec2 mouseFrac = vec2(mouse.x,resolution.y-mouse.y)/resolution;
  vec2 mouseCoord = vec2(mouse.x/resolution.y,mouse.y/resolution.y);
  vec2 mouseCoordCentered = vec2((mouse.x-(resolution.x/2.0))/resolution.y,(mouse.y-(resolution.y/2.0))/resolution.y);

//  eyePos.x = eyePos.x + mouseCoordCentered.x * eyeVertFov;
//  eyePos.y = eyePos.y + mouseCoordCentered.y * eyeVertFov;

//  eyeVertFov = eyeVertFov / (mouseCoord.y+0.01) * 1000.0;
//  eyeVertFov = eyeVertFov * (mouseCoord.y+0.01) ;
//  eyeVertFov = eyeVertFov * abs((sin(time/50))+0.1);

  float pixelHeight = eyeVertFov / resolution.y;
  float pixelWidth = pixelHeight;

  float aspectRatio = resolution.x/resolution.y;
  float eyeHorizFov = eyeVertFov*aspectRatio;
  vec2 worldCoord = eyePos+screenCoordCentered*eyeVertFov;

  vec4 nocolor = vec4(0.0,0.0,0.0,0.0);

  if(rmode == -1){
    float rn = rand(vec2((gl_FragCoord.x/resolution.x+mouseFrac.x)/2.0,(gl_FragCoord.y/resolution.y+mouseFrac.x)/2.0));
    vec4 value;

    if(rn < 0.5){
      value = off;
    }
    else{
      value = on;
    }

/*
    if(rn < 0.4){
      value = air;
    }
    else{

      if(rn < 0.6){
        value = water;
      }
      else{
        value = sand;
      }

    }
*/

    gl_FragColor = value;
  }
  else{
    bool aboveExists = gl_FragCoord.y < resolution.y-0.9;
    bool belowExists = gl_FragCoord.y > 0.9;
    bool leftExists = gl_FragCoord.x > 0.9;
    bool rightExists = gl_FragCoord.x < resolution.x-0.9;

    vec4 al,a,ar;
    vec4 l,c,r;
    vec4 bl,b,br;

    al = nocolor;
    a = nocolor;
    ar = nocolor;
    l = nocolor;
    c = nocolor;
    r = nocolor;
    bl = nocolor;
    b = nocolor;
    br = nocolor;

    bool alE,aE,arE;
    bool lE, rE;
    bool blE,bE,brE;

    alE = false;
    aE = false;
    arE = false;
    lE = false;
    rE = false;
    blE = false;
    bE = false;
    brE = false;

    c = texelFetch(tex0, ivec2(int(gl_FragCoord.x), int(gl_FragCoord.y)), 0 );

    if(aboveExists){
      aE = true;
      a = texelFetch(tex0, ivec2(int(gl_FragCoord.x), int(gl_FragCoord.y+1)), 0 );

      if(leftExists){
        alE = true;
        al = texelFetch(tex0, ivec2(int(gl_FragCoord.x-1), int(gl_FragCoord.y+1)), 0 );
      }

      if(rightExists){
        arE = true;
        ar = texelFetch(tex0, ivec2(int(gl_FragCoord.x+1), int(gl_FragCoord.y+1)), 0 );
      }

    }

    if(leftExists){
      lE = true;
      l = texelFetch(tex0, ivec2(int(gl_FragCoord.x-1), int(gl_FragCoord.y)), 0 );
    }

    if(rightExists){
      rE = true;
      r = texelFetch(tex0, ivec2(int(gl_FragCoord.x+1), int(gl_FragCoord.y)), 0 );
    }

    if(belowExists){
      bE = true;
      b = texelFetch(tex0, ivec2(int(gl_FragCoord.x), int(gl_FragCoord.y-1)), 0 );

      if(leftExists){
        blE = true;
        bl = texelFetch(tex0, ivec2(int(gl_FragCoord.x-1), int(gl_FragCoord.y-1)), 0 );
      }

      if(rightExists){
        brE = true;
        br = texelFetch(tex0, ivec2(int(gl_FragCoord.x+1), int(gl_FragCoord.y-1)), 0 );
      }

    }

/*
    if( int(gl_FragCoord.y) % 2 == rmode){

      if(aE){
        if(c.w < a.w) c = a;
      }

    }
    else{

      if(bE){
        if(b.w < c.w) c = b;
      }

    }
*/
    int count = 0;

    if(alE) if(al == on) count++;
    if(aE) if(a == on) count++;
    if(arE) if(ar == on) count++;
    if(lE) if(l == on) count++;
    if(rE) if(r == on) count++;
    if(blE) if(bl == on) count++;
    if(bE) if(b == on) count++;
    if(brE) if(br == on) count++;

    if(c == off){
      if(count == 3) c = on;    
    }
    else{
      c = off;
      if(count == 2) c = on;
      if(count == 3) c = on;
    }

    gl_FragColor = c;
  }
 
}
