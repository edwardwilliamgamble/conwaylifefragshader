import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.GL14;
import org.lwjgl.util.glu.GLU;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.EXTFramebufferObject.*;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.Util;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Main {
	static final int WINXRES = 512;
	static final int WINYRES = 512;
	// static final int WINXRES = 1280;
	// static final int WINYRES = 720;
	static final int FSXRES = 1920;
	static final int FSYRES = 1080;
	private boolean useShader = true;

	private int shader = 0;
	private int vertShader = 0;
	private int fragShader = 0;
	private int uni_resolution = -1;
	private int uni_time = -1;
	private int uni_mouse = -1;
	private int uni_tex0 = -1;
	private int uni_tex1 = -1;
	private int uni_cameraPosition = -1;
	private int uni_cameraTarget = -1;
	private int uni_sphereDesc = -1;
	private int uni_rmode = -1;

	static final int NUM_FBOS = 2;
	int[] framebufferTextureID = new int[NUM_FBOS];
	int[] framebufferID = new int[NUM_FBOS];
	int[] depthRenderBufferID = new int[NUM_FBOS];
	int currFbo = 0;
	int lastFbo = 1;

	long lastFrame;
	int fps;

	int rmode = -1;

	long lastFPS;

	long startTime;

	boolean vsyncEnabled = true;

	public void start() {

		try {
			Display.setDisplayMode(new DisplayMode(WINXRES, WINYRES));
			Display.setVSyncEnabled(vsyncEnabled);

			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		int currWidth = Display.getDisplayMode().getWidth();
		int currHeight = Display.getDisplayMode().getHeight();

		initGL(currWidth, currHeight); // init OpenGL
		getDelta(); // call once before loop to initialise lastFrame
		startTime = getTime(); // call before loop to initialise fps timer
		lastFPS = startTime; // call before loop to initialise fps timer

		while (!Display.isCloseRequested()) {
			int delta = getDelta();

			update(delta);
			currWidth = Display.getDisplayMode().getWidth();
			currHeight = Display.getDisplayMode().getHeight();
			renderGL(currWidth, currHeight);

			Display.update();
			// Display.sync(60); // cap fps to 60fps
		}

		Display.destroy();
	}

	public void update(int delta) {
		Mouse.poll();

		while (Keyboard.next()) {

			if (Keyboard.getEventKeyState()) {

				if (Keyboard.getEventKey() == Keyboard.KEY_F) {
					rmode = -1;

					if (Display.isFullscreen()) {
						setDisplayMode(WINXRES, WINYRES, false);
						initGL(WINXRES, WINYRES);
					} else {
						setDisplayMode(FSXRES, FSYRES, true);
						initGL(FSXRES, FSYRES);
					}

				} else if (Keyboard.getEventKey() == Keyboard.KEY_V) {
					vsyncEnabled = !vsyncEnabled;
					Display.setVSyncEnabled(vsyncEnabled);
				} else if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
					Display.destroy();
					System.exit(0);
				}

			}
		}

		updateFPS(); // update FPS Counter
	}

	public void initGL(int width, int height) {

		/*
		 * create the shader program. If OK, create vertex and fragment shaders
		 */
		shader = ARBShaderObjects.glCreateProgramObjectARB();

		if (shader != 0) {
			vertShader = createVertShader("shaders/screen.vert");
			fragShader = createFragShader("shaders/screen.frag");
		} else
			useShader = false;

		/*
		 * if the vertex and fragment shaders setup sucessfully, attach them to
		 * the shader program, link the shader program (into the GL context I
		 * suppose), and validate
		 */
		if (vertShader != 0 || fragShader != 0) {

			if (vertShader != 0) {
				ARBShaderObjects.glAttachObjectARB(shader, vertShader);
			}

			if (fragShader != 0) {
				ARBShaderObjects.glAttachObjectARB(shader, fragShader);
			}

			ARBShaderObjects.glLinkProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader,
					ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL11.GL_FALSE) {
				printLogInfo(shader);
				useShader = false;
			}
			ARBShaderObjects.glValidateProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader,
					ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL11.GL_FALSE) {
				printLogInfo(shader);
				useShader = false;
			}
		} else {
			useShader = false;
		}

		if (useShader) {
			uni_resolution = GL20.glGetUniformLocation(shader, "resolution");
			uni_time = GL20.glGetUniformLocation(shader, "time");
			uni_mouse = GL20.glGetUniformLocation(shader, "mouse");
			uni_cameraPosition = GL20.glGetUniformLocation(shader,
					"cameraPosition");
			uni_cameraTarget = GL20
					.glGetUniformLocation(shader, "cameraTarget");
			uni_sphereDesc = GL20.glGetUniformLocation(shader, "sphereDesc");
			uni_rmode = GL20.glGetUniformLocation(shader, "rmode");
		}

		// check if GL_EXT_framebuffer_object can be use on this system
		if (!GLContext.getCapabilities().GL_EXT_framebuffer_object) {
			System.out.println("FBO not supported!!!");
			System.exit(1);
		} else {
			// init our fbos
			for (int i = 0; i < framebufferID.length; i++) {
				framebufferID[i] = glGenFramebuffersEXT();
				framebufferTextureID[i] = glGenTextures();
				depthRenderBufferID[i] = glGenRenderbuffersEXT();
				glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebufferID[i]);
				glBindTexture(GL_TEXTURE_2D, framebufferTextureID[i]);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0,
						GL_RGBA, GL_INT, (java.nio.ByteBuffer) null);
				glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
						GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
						framebufferTextureID[i], 0);

				glBindRenderbufferEXT(GL_RENDERBUFFER_EXT,
						depthRenderBufferID[i]);
				glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,
						GL14.GL_DEPTH_COMPONENT24, width, height);
				glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,
						GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT,
						depthRenderBufferID[i]);
				glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
			}

		}

	}

	public void renderGL(int width, int height) {
		// FBO render pass
		glViewport(0, 0, width, height);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebufferID[currFbo]);
		glBindTexture(GL_TEXTURE_2D, framebufferTextureID[lastFbo]);

		if (useShader) {
			ARBShaderObjects.glUseProgramObjectARB(shader);
		}

		GL20.glUniform2f(uni_resolution, width * 1.0f, height * 1.0f);
		GL20.glUniform1f(uni_time, getRuntime() / 1000.0f);
		GL20.glUniform4f(uni_mouse, (float) Mouse.getX(), (float) Mouse.getY(),
				0.0f, 0.0f);

		GL20.glUniform4f(
				uni_cameraPosition,
				(width / 2.0f - (float) Mouse.getX()) / (width * 0.5f),
				0.0f,
				(height / 2.0f - (float) Mouse.getY()) / (height * 0.5f) - 3.0f,
				0.0f);
		GL20.glUniform4f(uni_cameraTarget, 0.0f, 0.0f, 0.0f, 0.0f);
		GL20.glUniform4f(uni_sphereDesc, 0.0f, 0.0f, 0.0f, 1.0f);
		GL20.glUniform1i(uni_rmode, rmode);

		drawBox(width, height); // draw the box

		rmode++;
		if (rmode > 1)
			rmode = 0;

		// release the shader
		ARBShaderObjects.glUseProgramObjectARB(0);

		// Normal render pass, draw cube with texture

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
		glBindTexture(GL_TEXTURE_2D, framebufferTextureID[currFbo]);
		glViewport(0, 0, width, height); // set The Current Viewport

		if (useShader) {
			ARBShaderObjects.glUseProgramObjectARB(shader);
		}

		GL20.glUniform2f(uni_resolution, width * 1.0f, height * 1.0f);
		GL20.glUniform1f(uni_time, getRuntime() / 1000.0f);
		GL20.glUniform4f(uni_mouse, (float) Mouse.getX(), (float) Mouse.getY(),
				0.0f, 0.0f);

		GL20.glUniform4f(
				uni_cameraPosition,
				(width / 2.0f - (float) Mouse.getX()) / (width * 0.5f),
				0.0f,
				(height / 2.0f - (float) Mouse.getY()) / (height * 0.5f) - 3.0f,
				0.0f);
		GL20.glUniform4f(uni_cameraTarget, 0.0f, 0.0f, 0.0f, 0.0f);
		GL20.glUniform4f(uni_sphereDesc, 0.0f, 0.0f, 0.0f, 1.0f);
		GL20.glUniform1i(uni_rmode, rmode);

		drawBox(width, height); // draw the box

		// rmode++;
		// if(rmode>1) rmode = 0;

		// release the shader
		ARBShaderObjects.glUseProgramObjectARB(0);

		glDisable(GL_TEXTURE_2D);
		glFlush();

		int temp = currFbo;
		currFbo = lastFbo;
		lastFbo = temp;
	}

	public void drawBox(int width, int height) {
		GL11.glColor3f(1.0f, 1.0f, 1.0f);// white
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0.0f, 0.0f);
		GL11.glVertex3f(-1.0f, 1.0f, 0.0f);
		GL11.glTexCoord2f(1.0f, 0.0f);
		GL11.glVertex3f(1.0f, 1.0f, 0.0f);
		GL11.glTexCoord2f(1.0f, 1.0f);
		GL11.glVertex3f(1.0f, -1.0f, 0.0f);
		GL11.glTexCoord2f(0.0f, 1.0f);
		GL11.glVertex3f(-1.0f, -1.0f, 0.0f);
		GL11.glEnd();
	}

	/**
	 * Set the display mode to be used
	 * 
	 * @param width
	 *            The width of the display required
	 * @param height
	 *            The height of the display required
	 * @param fullscreen
	 *            True if we want fullscreen mode
	 */
	public void setDisplayMode(int width, int height, boolean fullscreen) {

		// return if requested DisplayMode is already set
		if ((Display.getDisplayMode().getWidth() == width)
				&& (Display.getDisplayMode().getHeight() == height)
				&& (Display.isFullscreen() == fullscreen)) {
			return;
		}

		try {
			DisplayMode targetDisplayMode = null;

			if (fullscreen) {
				DisplayMode[] modes = Display.getAvailableDisplayModes();
				int freq = 0;

				for (int i = 0; i < modes.length; i++) {
					DisplayMode current = modes[i];

					if ((current.getWidth() == width)
							&& (current.getHeight() == height)) {
						if ((targetDisplayMode == null)
								|| (current.getFrequency() >= freq)) {
							if ((targetDisplayMode == null)
									|| (current.getBitsPerPixel() > targetDisplayMode
											.getBitsPerPixel())) {
								targetDisplayMode = current;
								freq = targetDisplayMode.getFrequency();
							}
						}

						// if we've found a match for bpp and frequence against
						// the
						// original display mode then it's probably best to go
						// for this one
						// since it's most likely compatible with the monitor
						if ((current.getBitsPerPixel() == Display
								.getDesktopDisplayMode().getBitsPerPixel())
								&& (current.getFrequency() == Display
										.getDesktopDisplayMode().getFrequency())) {
							targetDisplayMode = current;
							break;
						}
					}
				}
			} else {
				targetDisplayMode = new DisplayMode(width, height);
			}

			if (targetDisplayMode == null) {
				System.out.println("Failed to find value mode: " + width + "x"
						+ height + " fs=" + fullscreen);
				return;
			}

			Display.setDisplayMode(targetDisplayMode);
			Display.setFullscreen(fullscreen);
		} catch (LWJGLException e) {
			System.out.println("Unable to setup mode " + width + "x" + height
					+ " fullscreen=" + fullscreen + e);
		}
	}

	/**
	 * Calculate how many milliseconds have passed since last frame.
	 * 
	 * @return milliseconds passed since last frame
	 */
	public int getDelta() {
		long time = getTime();
		int delta = (int) (time - lastFrame);
		lastFrame = time;

		return delta;
	}

	/**
	 * Calculate how many milliseconds have passed since last frame.
	 * 
	 * @return milliseconds passed since last frame
	 */
	public int getRuntime() {
		long time = getTime();
		int delta = (int) (time - startTime);
		return delta;
	}

	/**
	 * Get the accurate system time
	 * 
	 * @return The system time in milliseconds
	 */
	public long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	/**
	 * Calculate the FPS and set it in the title bar
	 */
	public void updateFPS() {
		if (getTime() - lastFPS > 1000) {
			Display.setTitle("FPS: " + fps);
			fps = 0;
			lastFPS += 1000;
		}
		fps++;
	}

	public static void main(String[] argv) {
		Main main = new Main();
		main.start();
	}

	/*
	 * With the exception of syntax, setting up vertex and fragment shaders is
	 * the same.
	 * 
	 * @param the name and path to the vertex shader
	 */
	private int createVertShader(String filename) {
		// vertShader will be non zero if succefully created

		int vertShader = ARBShaderObjects
				.glCreateShaderObjectARB(ARBVertexShader.GL_VERTEX_SHADER_ARB);
		// if created, convert the vertex shader code to a String
		if (vertShader == 0) {
			return 0;
		}
		String vertexCode = "";
		String line;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			while ((line = reader.readLine()) != null) {
				vertexCode += line + "\n";
			}
		} catch (Exception e) {
			System.out.println("Fail reading vertex shading code");
			return 0;
		}
		/*
		 * associate the vertex code String with the created vertex shader and
		 * compile
		 */
		ARBShaderObjects.glShaderSourceARB(vertShader, vertexCode);
		ARBShaderObjects.glCompileShaderARB(vertShader);
		// if there was a problem compiling, reset vertShader to zero
		if (ARBShaderObjects.glGetObjectParameteriARB(vertShader,
				ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
			printLogInfo(vertShader);
			vertShader = 0;
		}
		// if zero we won't be using the shader
		return vertShader;
	}

	// same as per the vertex shader except for method syntax
	private int createFragShader(String filename) {

		int fragShader = ARBShaderObjects
				.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
		if (fragShader == 0) {
			return 0;
		}
		String fragCode = "";
		String line;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			while ((line = reader.readLine()) != null) {
				fragCode += line + "\n";
			}
		} catch (Exception e) {
			System.out.println("Fail reading fragment shading code");
			return 0;
		}
		ARBShaderObjects.glShaderSourceARB(fragShader, fragCode);
		ARBShaderObjects.glCompileShaderARB(fragShader);
		if (ARBShaderObjects.glGetObjectParameteriARB(fragShader,
				ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
			printLogInfo(fragShader);
			fragShader = 0;
		}
		return fragShader;
	}

	private static boolean printLogInfo(int obj) {
		IntBuffer iVal = BufferUtils.createIntBuffer(1);
		ARBShaderObjects.glGetObjectParameterARB(obj,
				ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB, iVal);

		int length = iVal.get();
		if (length > 1) {
			// We have some info we need to output.
			ByteBuffer infoLog = BufferUtils.createByteBuffer(length);
			iVal.flip();
			ARBShaderObjects.glGetInfoLogARB(obj, iVal, infoLog);
			byte[] infoBytes = new byte[length];
			infoLog.get(infoBytes);
			String out = new String(infoBytes);
			System.out.println("Info log:\n" + out);
		} else
			return true;
		return false;
	}

}