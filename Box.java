import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.Util;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Box {
    static int tick = 0;
    static double tickd = 0.0;
    private boolean useShader=true;
    private Texture texture0;
    private Texture texture1;
    private int shader=0;
    private int vertShader=0;
    private int fragShader=0;
    private int uni_resolution=-1;
    private int uni_time=-1;
    private int uni_mouse=-1;
    private int uni_tex0=-1;
    private int uni_tex1=-1;
    private int uni_cameraPosition=-1;
    private int uni_cameraTarget=-1;
    private int uni_sphereDesc=-1;

    public Box(){
        shader=ARBShaderObjects.glCreateProgramObjectARB();
        
        if(shader!=0){
            vertShader=createVertShader("shaders/screen.vert");
            fragShader=createFragShader("shaders/screen.frag");
        }
        else useShader=false;

        if(vertShader !=0 || fragShader !=0){

            if(vertShader !=0){
              ARBShaderObjects.glAttachObjectARB(shader, vertShader);
            }

            if(fragShader !=0){
              ARBShaderObjects.glAttachObjectARB(shader, fragShader);
            }

            ARBShaderObjects.glLinkProgramARB(shader);
            if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL11.GL_FALSE) {
                printLogInfo(shader);
                useShader=false;
            }
            ARBShaderObjects.glValidateProgramARB(shader);
            if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL11.GL_FALSE) {
                printLogInfo(shader);
                useShader=false;
            }
        }else useShader=false;

        if(useShader){
          uni_resolution = GL20.glGetUniformLocation(shader, "resolution");
          uni_time = GL20.glGetUniformLocation(shader, "time");
          uni_mouse = GL20.glGetUniformLocation(shader, "mouse");
          uni_cameraPosition = GL20.glGetUniformLocation(shader, "cameraPosition");
          uni_cameraTarget = GL20.glGetUniformLocation(shader, "cameraTarget");
          uni_sphereDesc = GL20.glGetUniformLocation(shader, "sphereDesc");
        }

    }

    public void draw(){

        if(useShader) {
            ARBShaderObjects.glUseProgramObjectARB(shader);
        }

        GL20.glUniform2f(uni_resolution, 1920.0f, 1080.0f);
//        GL20.glUniform2f(GL20.glGetUniformLocation(shader, "resolution"), 800.0f, 600.0f);
        GL20.glUniform1f(uni_time, (float)tickd);
        GL20.glUniform4f(uni_mouse, (float)Mouse.getX(), (float)Mouse.getY(), 0.0f, 0.0f);

//        GL20.glUniform4f(uni_cameraPosition, (float)Math.sin(tickd*0.1), 0.0f, (float)Math.cos(tickd*0.1)+3.0f, 0.0f);
        GL20.glUniform4f(uni_cameraPosition, (1920.0f/2.0f-(float)Mouse.getX())/1000.0f, 0.0f, (1080.0f/2.0f-(float)Mouse.getY())/500.0f-3.0f, 0.0f);
        GL20.glUniform4f(uni_cameraTarget, 0.0f, 0.0f, 0.0f, 0.0f);
        GL20.glUniform4f(uni_sphereDesc, 0.0f, 0.0f, 0.0f, 1.0f);

        //GL11.glLoadIdentity();
        //GL11.glTranslatef(0.0f, 0.0f, -10.0f);
        //GL11.glColor3f(1.0f, 1.0f, 1.0f);//white
        //GL11.glColor3f((float)Math.sin(tickd), (float)Math.tan(tickd), (float)Math.cos(tickd));//white
        //GL11.glColor3f((float)Math.sin(tickd), 0.0f, 0.0f);

        GL11.glColor3f(1.0f, 1.0f, 1.0f);//white

        GL11.glBegin(GL11.GL_QUADS);
        GL11.glTexCoord2f(0.0f, 0.0f);
        GL11.glVertex3f(-1.0f, 1.0f, 0.0f);
        GL11.glTexCoord2f(1.0f, 0.0f);
        GL11.glVertex3f(1.0f, 1.0f, 0.0f);
        GL11.glTexCoord2f(1.0f, 1.0f);
        GL11.glVertex3f(1.0f, -1.0f, 0.0f);
        GL11.glTexCoord2f(0.0f, 1.0f);
        GL11.glVertex3f(-1.0f, -1.0f, 0.0f);
        GL11.glEnd();

        //release the shader
        //ARBShaderObjects.glUseProgramObjectARB(0);
        tick++;
        tickd+=0.1;
    }

    private int createVertShader(String filename){
        //vertShader will be non zero if succefully created

        int vertShader=ARBShaderObjects.glCreateShaderObjectARB(ARBVertexShader.GL_VERTEX_SHADER_ARB);
        //if created, convert the vertex shader code to a String
        if(vertShader==0){return 0;}
        String vertexCode="";
        String line;
        try{
            BufferedReader reader=new BufferedReader(new FileReader(filename));
            while((line=reader.readLine())!=null){
                vertexCode+=line + "\n";
            }
        }catch(Exception e){
            System.out.println("Fail reading vertex shading code");
            return 0;
        }

        ARBShaderObjects.glShaderSourceARB(vertShader, vertexCode);
        ARBShaderObjects.glCompileShaderARB(vertShader);
        //if there was a problem compiling, reset vertShader to zero
        if (ARBShaderObjects.glGetObjectParameteriARB(vertShader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
            printLogInfo(vertShader);
            vertShader=0;
        }
        //if zero we won't be using the shader
        return vertShader;
    }

    private int createFragShader(String filename){

        int fragShader=ARBShaderObjects.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
        if(fragShader==0){return 0;}
            String fragCode="";
            String line;
        try{
            BufferedReader reader=new BufferedReader(new FileReader(filename));
            while((line=reader.readLine())!=null){
                fragCode+=line + "\n";
            }
        }catch(Exception e){
            System.out.println("Fail reading fragment shading code");
            return 0;
        }
        ARBShaderObjects.glShaderSourceARB(fragShader, fragCode);
        ARBShaderObjects.glCompileShaderARB(fragShader);
        if (ARBShaderObjects.glGetObjectParameteriARB(fragShader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
            printLogInfo(fragShader);
            fragShader=0;
        }
        return fragShader;
    }
    
    private static boolean printLogInfo(int obj){
        IntBuffer iVal = BufferUtils.createIntBuffer(1);
        ARBShaderObjects.glGetObjectParameterARB(obj,ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB, iVal);

        int length = iVal.get();
        if (length > 1) {
            // We have some info we need to output.
            ByteBuffer infoLog = BufferUtils.createByteBuffer(length);
            iVal.flip();
            ARBShaderObjects.glGetInfoLogARB(obj, iVal, infoLog);
            byte[] infoBytes = new byte[length];
            infoLog.get(infoBytes);
            String out = new String(infoBytes);
            System.out.println("Info log:\n"+out);
        }
        else return true;
        return false;
    }

}